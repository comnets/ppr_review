# A Review of Network Coding for Partial Packet Recovery in Wireless Networks
This paper will be submitted to International Journal of Electrical and Computer Engineering (IJECE) which is a **SCOPUS** index journal with SJR 2017: 0.296, SNIP 2017: 1.001, Q2 on Computer Science, and Q2 on Electrical & Electronics Engineering.

* https://www.scimagojr.com/journalsearch.php?q=21100373959&tip=sid
* https://www.iaescore.com/journals/index.php/IJECE
