# A Review of Network Coding for Partial Packet Recovery in Wireless Networks

This paper **might** be submitted to **ICOICT 2019** where it will be held on 24th-26th July 2019 in Kuala Lumpur, Malaysia.

* Link of conference: https://www.icoict.org/
* Submission deadline: **March 15th 2018** 
* Acceptance notification: **April 15th 2019**
* Camera-ready version: **May 15th 2019**
