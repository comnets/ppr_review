# Partial Packet Recovery with Network Coding in Wireless Networks: A Review

This paper is prepared for **Electronics** from MDPI

* Link of journal: https://www.mdpi.com/journal/electronics
* Scimago: https://www.scimagojr.com/journalsearch.php?q=21100829272&tip=sid&clean=0
* Ranking: Q1 Journal 
* SJR 2017: 0.55
