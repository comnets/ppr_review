%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!t]
	\centering
	\includegraphics[width=3.5in]{figures/wirr_comm_sys.png}
	\caption{Packet transmissions in wireless communications: packet contains error bits and has false CRC status is known as the partial packet, meanwhile packet with no error and has correct CRC status is called the valid packet. PPR exploits partial packets that have some useful information to recover the errors.} 
	\label{fig:comm_sys&encoder}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Packet Recovery in Wireless Networks}\label{sec:recovery}

In this section, we describe the importance of partial packet recovery in wireless networks. However, firstly, Subsection \ref{subsec:error} is going to begin this section by presenting the error in wireless networks along with its error control. Secondly, Subsection \ref{subsec:ppr} continue the section by giving the details of partial packet recovery. Next, Subsection \ref{subsec:diversity} provides a spatial diversity perspective in wireless networks and partial packets. Finally, Subsection \ref{subsec:ppr-netcod} ends this section by relating network coding to partial packet recovery issues.

\subsection{Error Control}\label{subsec:error}

Due to the nature of wireless channel,  packet errors has become one of the inevitable topics that need to be tackled in wireless telecommunication systems. Errors in packets are caused by signal impairment, meaning that the sent signal at the sender is not the same as the received signal at the receiver. There are several causes that produce impairment in wireless transmissions \cite{Bash2012, Stallings2014}. For example, attenuation, that is the signal strength goes down with distance when travels through a medium transmission, can introduce bit errors. A weak signal is vulnerable with noise and needs to maintain its strength higher than noise, otherwise, the receiver cannot detect and interpret the signal correctly. Noise, by definition, is undesired signals that are added between transmission and reception. Noise, as well, is another reason for impairment. Another example, that causes errors, is delay distortion. When a sequence of bits is being transmitted, the bits need to arrive at the receiver in the right order. If a delay happens, it may affect the bit order at the receiver and can cause inter-symbol interference. 

Automatic-repeat-request (ARQ) and forward-error-control (FEC) scheme are the two common techniques that have been implemented for controlling error and improving the quality transmission in wireless networks \cite{Shu2004}. FEC uses error-correcting codes to detect and correct errors. Whenever the receiver notice that there is an error, it will try to find its location and correct the error. Meanwhile, in ARQ, a good error detection code is used. The receiver can only detect the presence of error without being able to correct it. For correction of the corrupted packet, the receiver will send a request to the sender to re-transmit the packet. The sender will always re-send the requested packet until the receiver gets it without errors. The latest scheme, that involves ARQ and FEC, is Hybrid-ARQ (HARQ) which is a combination of both of them. In HARQ, a subsystem of FEC is placed in ARQ system. The purpose of FEC in this design is to cut the number of retransmissions down by correcting the errors. As a result, the throughput can be improved.

%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!t]
	\centering
	\includegraphics[width=3.5in]{figures/ppr-class}
	\caption{PPR protocol classification: some of the protocols are classified to either PHY-layer Information or Non PHY-layer Information. Also, there are several techniques in Non PHY-layer Information group, such as checksum and block-based, spatial diversity, and network coding.} 
	\label{fig:ppr-class}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

However, using ARQ, FEC or HARQ is not enough. Wireless networks have unstable link condition and packet losses are the common case \cite{Aguayo2004,Xu2012}. In fact, Cabrera et al \cite{Cabrera2017} showed that for 54 Mbps in a multicast transmission, 46\% of the received packets contain errors. The authors conducted the experiment using a Raspberry Pi 2 with IEEE 802.11g protocol and a laptop that is connected, wirelessly, to Linksys WRT54GL Wireless Router to investigate the behavior of wireless networks. It has been observed that in some partial packets, i.e. packets consist of bit errors and have false cyclic-redundancy-check (CRC), there is some useful information which can be used. Therefore, the recovery of partial packets may improve the throughput of wireless networks and decrease the number of transmissions.

\subsection{Partial Packet Recovery}\label{subsec:ppr}

In wireless data transmissions, there are two kinds of packets, namely a valid packet, and an invalid packet as shown in Figure \ref{fig:comm_sys&encoder}. The valid packet is a packet that has the correct cyclic redundancy code (CRC) status. Before sender transmits packets, it will put a CRC code, a mechanism for error detection, to each packet. Then, the receiver needs to make sure that all the packets are error-free by checking the CRC status of each packet when it receives the packets. Meanwhile, invalid packet, known as the partial packet, is a packet that contains error and it has false CRC status. For preventing the error bits to be passed to the higher layer (e.g. data link layer), invalid packets will, mainly, be dropped at the physical layer in the standard protocol of wireless communications. The number of bit errors in partial packets is different and it depends on the channel conditions. However, most of the time, partial packets contain some useful information and it can be exploited to improve the network performance with partial packet recovery (PPR) schemes \cite{Sindhu1977}.

PPR is more effective and efficient in wireless networks than in wired network cases. In fact, PPR may be useless in wired networks. There are several reasons for this issue \cite{Xu2012}. First, link communications in wired networks are more reliable than in wireless networks. Therefore, the error rate is lower in wired than in wireless. Moreover, the error in wired networks is, mainly, caused by the congestion at intermediate links. As a result, wired network transmission comes with all of bits or nothing, meaning that receiver at the destination can only receive all bits of packets or none from the sender. This reason makes PPR is suitable in wireless networks where the error rate is higher and partial packets are present. Second, PPR introduces, in main cases, overheads. These overheads can affect the throughput by causing longer waiting time. As a consequence, it will reduce performance. Therefore, the cost of implementing PPR will overwhelm the benefit it brings.

Partial packet recovery (PPR) in wireless networks has been studied in the last two decades and many techniques have been proposed. In general, PPR methods are categorized into two main groups namely PPR with PHY-layer information and PPR without PHY-layer information, respectively.

%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!t]
	\centering
	\includegraphics[width=3.5in]{figures/ppr-protocol.png}
	\caption{Trend of PPR protocol: in two decades, there were approximately twenty-four PPR protocols have been proposed. Most of these protocol were deployed in the real hardware such as USRP, commercial WLAN cards, TelosB, B-MAC, and Texas Instrument CC2500. Only a few protocols were implemented in simulation.} 
	\label{fig:ppr-protocol}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{PPR with PHY-layer Information}

The concept of this solution is based on the use of soft values from the physical layer. A confidence measure of 0-1 in received data bits is computed by PHY-layer. These values are used in decoding and its usage is normally called soft-decoding \cite{Woo2007}. Soft-decoding can distinguish between error and corrected bits in PHY-layer and send its confidence values to the upper layer (e.g. data link layer). Then the upper layer will send a request for retransmission to the sender. This retransmission request is only for the part of packets that contains error bits. In other words, the sender will not send the entire packet but only send the error part of the packets.

PPR with PHY-layer Information (PLI) requires physical-layer to pass the confidence values of packets to the data-link layer. However, in the current protocol of wireless systems, it is impossible. After calculating the confidence values, the physical layer will not be able to send it to the upper layer and drop them. Therefore, it becomes challenging to implement PPR with PLI in the current wireless standard protocol without modifying the physical-layer because it needs modification on the hardware level.

The limitation of accessing physical-layer in standard commercial WiFi hardware, such as Atheros, Broadcom, Realtek, and Intel, makes PPR with PLI difficult in deployment. Thus, most of the implementations are deployed with Universal Software Radio Peripheral (USRP) devices. In fact, USRP is a common real device used for implementing new design protocols at the physical layer. Proposed schemes of PPR with PLI, for instance: PP-ARQ \cite{Jamieson2007}, MIXIT \cite{Katti2008}, SOFT \cite{Woo2007}, and CodeRepair \cite{Huang2015}, have been deployed with USRP devices \cite{Irianto2019}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure*}[!ht]
	\centering
	\subfloat[ACR: step 1]{\includegraphics[width=2.2in]{figures/acr_step_1.pdf}\label{fig:acr_1}}
	\hfil
	\subfloat[ACR: step 2]{\includegraphics[width=2.2in]{figures/acr_step_2.pdf}\label{fig:acr_2}} 
	\hfil\\
	\subfloat[ACR: step 3]{\includegraphics[width=2.2in]{figures/acr_step_3.pdf}\label{fig:acr_3}}
	\hfil
	\subfloat[ACR: step 4]{\includegraphics[width=2.2in]{figures/acr_step_4.pdf}\label{fig:acr_4}}
	\caption{Steps in algebraic consistency rule (ACR) check: ACR uses some properties of network coding for finding error locations and correcting the errors with multiple rounds of check mechanism. It consists of four steps, such as choose a subset of received encoded packets, guess the original packets, re-encode the non-chosen packet, and compare the re-encoded packets with the received encoded packets. \cite{Irianto2019}}\label{fig:acr_steps}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{PPR without PHY-layer Information}

In this approach, the confidence value is not used.  Instead, PPR without PLI optimizes the data-link layer to recover of partial packets. There are several reasons for not using the confidence value. For example, to get its value, a high computation is needed and it takes resources and times. Moreover, it is, currently, impossible to send the confidence value to the upper layer without modifying the standard protocol. Therefore, PPR with PHY-layer information cannot be deployed in the current commercial wireless cards. In contrast, PPR without PLI is, easily, implemented using some of the commercial wireless cards. However, it needs to have access to the data-link layer and it can be performed by tweaking the driver of wireless cards at the software level. Many solutions of PPR without PLI have been implemented in actual 802.11 networks with standard commercial WiFi hardware \cite{Lin2008,Iyer2009,Lu2009,Han2010,Hu2011,Zhang2011,Meer2012,Badam2011}.

\subsection{Spatial Diversity}\label{subsec:diversity}
In wireless networks, a node may have two or more antennas. For example, one antenna is responsible for transmitting and another antenna is for receiving data. In fact, two or more antennas could be dedicated only for transmitting or receiving. Spatial diversity is a common technique for combating fading issues. It can be implemented at the transmitter, receiver, or any node that has multiple antennas \cite{ShiCheng2005}. For instance, multiple input multiple outputs (MIMO) is one the spatial diversity implementation. MIMO requires multiple antennas on both sender and receiver. Another implementation of spatial diversity is known as cooperative communication \cite{Diggavi2004,Laneman2004} or virtual MIMO. In cooperative diversity, the sender can select on the near relays to help to transmit its data. The relay selection is based on the channel conditions and its performance. Thus, the sender has more options in choosing the relay for transmissions. However, most of these works in \cite{Diggavi2004,Laneman2004} is only theoretical with no implementation or experimental results.

Spatial diversity and partial packet recovery (PPR) are similar. Both of them are trying to combine something to improve their performance. For example, spatial diversity combines the signals at the physical layer meanwhile PPR combines multiple versions of the same coded packet at some higher layer \cite{Xu2012}. Several protocols of PPR exploited spatial diversity have been proposed, such as SpaC \cite{Ferriere2006}, MRD \cite{Miu2005}, and Relay-Assisted \cite{Luo2010}. In addition, some investigations and analyses using spatial diversity for partial packet have been presented in \cite{ShiCheng2005,Diggavi2004,Laneman2004,Valenti2003}


\subsection{Partial Packet Recovery and Network Coding}\label{subsec:ppr-netcod}

The first work, implemented network coding for partial packet recovery and called Packetized Rateless Algebraic Consistency (PRAC), was proposed by Angelopoulos et at \cite{Angelopoulos2014}. They utilize one of the network coding properties, which is the consistency rule, to recover error bits in partial packets. The key to this protocol is the use of algebraic consistency rule (ACR) check. ACR is able to find the location of error bits and correct the errors. The correction process involves multiple iterations of ACR. This work showed the capability and possibility of network coding in PPR problems. In fact, network coding offers better performance than the non-network coding approaches in recovering the error bits in partial packet issues.

Since then, several protocols involved network coding for PPR
have been proposed. For example, PPR-CS in \cite{Mohammadi2016} combines random linear network coding and compressive sensing. The authors retrieve error bits in partial packets by solving a set of sparse recovery (SR) problems. An algorithm, which is proposed by Candes and Tao in \cite{Candes2005} and based on sparse recovery, is used for error correction. Another protocol is DAPRAC \cite{Cabrera2017}, which is an improved version of PRAC. DAPRAC enhances the error correction process in PRAC. It uses a brute-force method in correcting errors by performing the permutation of corrupted bits. As a result, DAPRAC could reduce the error correction time in PRAC. The latest protocol of PPR with network coding, called S-PRAC and proposed in \cite{Irianto2019}, focuses more on very noisy wireless channels. S-PRAC is another version of DAPRAC. However, S-PRAC is superior to DAPRAC in noisy channel conditions. It elaborates the packets into segments to improve the time in finding the error locations and correcting the error bits. Therefore, S-PRAC is faster than DAPRAC when the number of errors is high.


\begin{table*}[!th]
	\caption{PPR Protocols in Wireless Networks}
	\centering
	\begin{tabular}{p{2cm} p{4cm} p{5cm} p{5cm}}
		\hline
		Protocol & Testbed / Simulation & Technique & PPR Classification \\
		\hline\hline
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		SOFT \cite{Woo2007}	& USRP, GNU Radio Software, WLAN based on 80.11 	& Combining algorithm, cross-layer architecture & PHY-layer Information and Spatial Diversity \\
		PP-ARQ \cite{Jamieson2007} & GNU Radio, Telos motes with 2.5 GHz, TinyOS, CC2420 radio, USRP, Matlab, Zigbee 802.15.4 & SoftPHY interface, postamble decoding	& PHY-layer Information \\
		MIXIT \cite{Katti2008} & USRP, Zigbee software radio & SoftPHY hints, network coding, diversity	& PHY-layer Information, Network Coding, and Spatial Diversity \\
		CodeRepair \cite{Huang2015} & USRP, GNU Radio Software, WLAN based on 802.11 & Convolutional codes, bit padding, correct error at the PHY layer 	& PHY-layer Information \\
		SmartPilot \cite{Qi2015} & USRP, GNU Radio Software, WLAN based on 802.11 & Adaptive hard pilot, reliable soft pilot, physical layer hints & PHY-layer Information \\
		SMR \cite{Cai2016} & USRP & Smart transmission, CSI precedence principle, MIMO 	& PHY-layer Information \\
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		Seda \cite{Ganti2006} & MicaZ, TinyOS &  Checksum and Blocks	&  Non PHY-layer Information  \\
		ZipTx \cite{Lin2008} & Midwifi driver, WLAN 802.11 card, Indoor and Outdoor experiment  &  Autorate algorithm, Reed-Solomon FEC	& Non PHY-layer Information \\
		FRJ \cite{Iyer2009} & Midwifi driver, Click toolkit, PC with WLAN card & Checksum, blocks, rate adaptation, frame size selection 	& Non PHY-layer Information \\
		PRO \cite{Lu2009} & WiFi 802.11 & Distributed relay selection, local qualification process, channel reciprocity, enhanced distributed channel access (EDCA),  opportunistic retransmission 	& Spatial Diversity and non PHY-layer Information \\
		Maranello \cite{Han2010} & OpenFWWF, OpenWRT, WLAN cards, Simulation & block-based recovery, Fletcher-32 Checksum 	& non PHY-layer Information \\
		pMORE \cite{Hu2011}	& Cisco Aironet, Madwifi driver, WiFi 802.11a/b/g &  Network coding, opportunistic routing, checksum and blocks & non PHY-layer Information \\
		UNITE \cite{Zhang2011} & Cisco Airnote, Madwifi driver, WiFi 802.11 & EC-based and Block-based, AMPS 	& non PHY-layer Information \\
		HTPPR \cite{Badam2011} & Simulation & Using tortoise link and hare link, chunk-based recovery, fuzzy, autorate & non PHY-layer Information \\
		H-Seda \cite{Meer2012} & TelosB with Chipcon-CC2420, IEEE 802.15.4, TinyOS & Checksum and dynamic block size 	& non PHY-layer Information \\
		ReSel \cite{Lu2012}	 & PC computer, iperf software & Block-based recovery, rate selection, NACK packet 	& non PHY-layer Information \\		
		Green-Frag \cite{Daghistani2015a} & TelosB with Chipcon-CC2420, IEEE 802.15.4, TinyOS & smaller frame blocks 	& non PHY-layer Information \\
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		PRAC \cite{Angelopoulos2014} & Texas Instrument CC2500 with 2,4 GHz, PC computer & network coding, algebraic concistency rule (ACR) & non PHY-layer Information and Network Coding \\
		PPR-CS \cite{Mohammadi2016}  & Simulation & network coding, sparse recovery algorithm, compressed sensing, markov chain 	& non PHY-layer Information and Network Coding \\
		DAPRAC \cite{Cabrera2017} & Simulation & network coding, algebraic concistency rule (ACR) 	& non PHY-layer Information and Network Coding \\
		S-PRAC \cite{Irianto2019} & Simulation & network coding, algebraic concistency rule (ACR), checksum, blocks 	& non PHY-layer Information and Network Coding \\
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		MRD \cite{Miu2005} & PC with WLAN card 802.11a/b/g, Madwifi driver & Coordinate receptions, frame combining, request-for-acknowledgment, path diversity 	& Spatial Diversity  \\	
		SpaC \cite{Ferriere2006} & B-MAC, TinyOS, wireless sensor networks & Packet combining, cooperative diversity, repetition coding, systematic and block codes 	& Spatial Diversity \\
		Relay-Assisted \cite{Luo2010} & Simulation & IDMA (Interleave-division multiple-access) 	& Spatial Diversity \\
		\hline
	\end{tabular}\label{table:ppr}
\end{table*}