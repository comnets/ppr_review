import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2014', '2015', '2016', '2017', '2019', )
y_pos = np.arange(len(objects))
performance = [2,1,2,2,2,2,2,3,1,3,2,1,1]

plt.barh(y_pos, performance, align='center', alpha=0.5, color='b')
plt.yticks(y_pos, objects)
plt.xlabel('Number of PPR Protocols')
plt.ylabel('Year')
plt.grid(True, linestyle='--', which='major',
                   color='grey', alpha=.25)

plt.tight_layout()
plt.savefig('ppr-protocol.png', dpi = 200)

plt.show()